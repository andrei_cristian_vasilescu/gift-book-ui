import axios from 'axios';

const SERVER = "http://localhost:8080"

class Api {

    static getBooks() {
        return axios.get(`${SERVER}/api/books`);
    }

    static getCategories() {
        return axios.get(`${SERVER}/api/categories`);
    }

    static getUsers() {
        return axios.get(`${SERVER}/api/users`);
    }

    static logIn(email, password) {
        return axios.post(`${SERVER}/login`, { email, password });
    }

    static register(user) {
        return axios.post(`${SERVER}/register`, user);
    }

    static deleteBook(bookId, token) {
        return axios.delete(`${SERVER}/api/books/${bookId}`,{
            headers: { 'Authorization': `Bearer ${token}`}
        });
    }

    static addBook(book, token) {
        return axios.post(`${SERVER}/api/books`, book, {
            headers: { 'Authorization': `Bearer ${token}`}
        });
    }

    static editBook(bookId, book, token) {
        return axios.put(`${SERVER}/api/books/${bookId}`, book, {
            headers: { 'Authorization': `Bearer ${token}`}
        });
    }
    
    static addCategory(category, token) {
        return axios.post(`${SERVER}/api/categories`, category, {
            headers: { 'Authorization': `Bearer ${token}`}
        });
    }

    static deleteCategory(categoryId, token) {
        return axios.delete(`${SERVER}/api/categories/${categoryId}`,{
            headers: { 'Authorization': `Bearer ${token}`}
        });
    }

    static editCategory(categoryId, category, token) {
        return axios.put(`${SERVER}/api/categories/${categoryId}`, category, {
            headers: { 'Authorization': `Bearer ${token}`}
        });
    }
}

export default Api;